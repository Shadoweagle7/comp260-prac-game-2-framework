﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PuckControl : MonoBehaviour {

    public AudioClip WallCollisionAudioClip;
    public AudioClip PaddleCollisionAudioClip;
    public LayerMask PaddleLayer;

    private AudioSource Audio;
    

	// Use this for initialization
	void Start () {
        Audio = GetComponent<AudioSource>();
	}
	
    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("OnCollisionEnter(): " + collision.gameObject.name);

        if (PaddleLayer.Contains(collision.gameObject))
        {
            Audio.PlayOneShot(PaddleCollisionAudioClip);
        }
        else
        {
            Audio.PlayOneShot(WallCollisionAudioClip);
        }
    }

    void OnCollisionStay(Collision collision)
    {
        Debug.Log("OnCollisionStay(): " + collision.gameObject.name);
    }

    void OnCollisionExit(Collision collision)
    {
        Debug.Log("OnCollisionExit(): " + collision.gameObject.name);
    }

	// Update is called once per frame
	void Update () {
		
	}
}

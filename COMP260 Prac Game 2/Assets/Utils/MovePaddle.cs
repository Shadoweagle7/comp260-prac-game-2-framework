﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour {
    private Rigidbody rigidBody;
    private Vector3 currMousePosition;

    public float Speed = 20f, Force = 10f;

    private Vector3 dir;

	// Use this for initialization
	void Start () {
        this.rigidBody = GetComponent<Rigidbody>();
        this.dir = new Vector3(0, 0, 0);
        this.rigidBody.useGravity = false;
	}
	
	// Update is called once per frame
	void Update () {
        Debug.Log("Time: " + Time.time);
        this.currMousePosition = GetMousePosition();

        // Set to true to enable testing of velocity, position and AddForce()
        if (true) {
            // velocity testing
            float horizontalInput = Input.GetAxis("Horizontal"),
                verticalInput = Input.GetAxis("Vertical");

            this.dir.x = horizontalInput;
            this.dir.z = verticalInput;

            Vector3 velocity = dir.normalized * this.Speed;

            // AddForce() testing
            if (horizontalInput > 0)
            {
                //this.rigidBody.AddForce(new Vector3(0.1f, 0, 0), ForceMode.VelocityChange);
                // this.rigidBody.position += new Vector3(0.1f, 0, 0);
            }
            else if (horizontalInput < 0)
            {
                //this.rigidBody.AddForce(new Vector3(-0.1f, 0, 0), ForceMode.VelocityChange);
            }

            if (verticalInput > 0)
            {
                //this.rigidBody.AddForce(new Vector3(0, 0, 0.1f), ForceMode.VelocityChange);
            }
            else if (verticalInput < 0)
            {
                //this.rigidBody.AddForce(new Vector3(0, 0, -0.1f), ForceMode.VelocityChange);
            }

            // Uncomment to use velocity from keyboard controller
            //this.rigidBody.velocity = velocity;
        }
    }

    private void FixedUpdate()
    {
        Debug.Log("Fixed Time: " + Time.fixedTime);

        Vector3 dir = this.currMousePosition - rigidBody.position;


        // this.rigidBody.AddForce(dir.normalized * this.Force);


        Vector3 velocity = dir.normalized * this.Speed;
        float move_distance = Speed * Time.fixedDeltaTime;
        float distanceToTarget = dir.magnitude;

        if (move_distance > distanceToTarget)
        {
            velocity = velocity * distanceToTarget / move_distance;
        }

        // Uncomment to start using mouse again like a normal person
        rigidBody.velocity = velocity;
    }

    private Vector3 GetMousePosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); // Calculates ray from mouse position

        Plane plane = new Plane(Vector3.up, Vector3.zero); // XZ plane
        float distance = 0;
        plane.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Vector3 pos = GetMousePosition();
        Gizmos.DrawLine(Camera.main.transform.position, pos);
    }
}
